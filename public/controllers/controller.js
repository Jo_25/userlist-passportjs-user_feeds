function AppCtrl($scope,$http)
{
	//console.log("hello world from controller");

	var refresh = function()
	{
	$http.get('/userlist').success(function(response)
	{
		//console.log("i got the data i requested");
		$scope.userlist = response;
		$scope.user = "";

	});
	};
	refresh();

	$scope.addContact = function()
	{
		console.log($scope.user);
		$http.post('/userlist', $scope.user).success(function(response)
		{
			console.log(response);
			refresh();
		})};
	

	$scope.remove = function(id)
	{
		console.log(id);
		$http.delete('/userlist/' + id).success(function(response)
		{
			refresh();

		});	
	};

	
	
	$scope.edit = function(id)
	{
		console.log(id);
		$http.get('/userlist/' + id).success(function(response)
		{
			$scope.user = response;

		});
	};
	
	$scope.update = function()
	{

		//console.log($scope.user._id);
		$http.post('/userlist/' + $scope.user._id, $scope.user).success(function(response)
			//$http.put('/userlist/' + $scope.user._id).success(function(response)
		{
			refresh();
		})
	};
	$scope.deselect = function()
	{
		$scope.user = "";
	}

}	