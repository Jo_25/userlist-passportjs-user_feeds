var express  = require('express');
var cookieSession = require('cookie-session');
var cookieParser = require('cookie-parser');
var app = express();
const flash = require("connect-flash");
var mongojs = require('mongojs');
var db = mongojs('litmus-users'); 
var session = require('cookie-session');
var ObjectID = require('mongodb').ObjectID;

const MongoClient = require('mongodb').MongoClient;
var bodyParser =  require('body-parser');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;


var url = 'mongodb://localhost:27017';

app.use(express.static(__dirname + '/public/'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(flash());



app.use(session({secret: "cats", resave: false, saveUninitialized: true}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());


passport.use(new LocalStrategy(
     function(username, password, done) {
     	console.log('in password')
     	var url = 'mongodb://localhost:27017/litmus-users';
         MongoClient.connect(url, (err, db) => {
             if(err) {
                 throw err;
             } 
             db.collection('managers').find({username: username}).toArray().then((user) => {
                 console.log(user);
                 if (err) { return done(err); }
                 if (user.length < 0)
                //if(user==0)
                 //if(!user)
                  {
                 	console.log('user not find');
                     return done(null, false, { message: 'Incorrect username.' });
                 }
                 if (user[0].password !== password) {
                 	console.log('password not matched');
                     return done(null, false, { message: 'Incorrect password.' });
                 }
                 console.log('Success'+JSON.stringify(user[0]));
                return done(null, user[0]);
             }, (err) => {
                 throw err;
             });
             db.close();
         });
     })
 );


passport.serializeUser(function(user, done) {
    console.log('Serialize user:'+JSON.stringify(user));
    done(null, user);

});
  
passport.deserializeUser(function(user, done) {
    console.log("Deserialize called...",user);
    done(null, user);
});

//post
app.post('/login',
  passport.authenticate('local', { successRedirect: '/success',
                                   failureRedirect: '/failed',
                                   failureFlash: true })
  );



	app.get('/success', (req, res) => 
	{
		res.sendFile(__dirname+ '/public/details.html');
	});


	app.get('/current-user', (req, res) => 
	{
		//res.send(req.user);
		res.status(200).send(JSON.stringify(req.user));
	});




	
	app.get('/failed', (req, res) =>
    {
		res.status(401).send('Invalid User');

	});
	 
	
	app.get('/details',function(req,res)
	{
		
		   if(req.user.access.has_access === "false")
		    {
		        res.send('Access Denied');
		    }
		    
		    		var brand_id = new ObjectID(req.user.access.brands_id);
		    		db.collection('feeds').find({brands_id: brand_id},function(err,doc)
		    		{
		            res.json(doc);
		    		});   		 
	}) 
	
	app.get('/userlist',function(req,res) 
	{
	console.log("I received a GET request")
	
		db.users.find(function (err, doc)
		{
		console.log(doc);
		res.json(doc);
		})
	}); 
	
	app.post('/userlist', function(req,res)
	{
		console.log(req.body);
		db.users.insert(req.body, function(err,doc)
		{
		res.json(doc);
		})
	});

	// for deleting data
	app.delete('/userlist/:id', function(req,res)
	{
	var id = req.params.id;
	console.log(id);
	db.users.remove({_id: mongojs.ObjectId(id)}, function(err, doc){
		res.json(doc);
	});
	});
	
	// Edit button
	app.get('/userlist/:id', function(req, res)
	{
		var id = req.params.id;
		console.log(id);
		db.users.findOne({_id: mongojs.ObjectId(id)}, function(err, doc)
			{
			res.json(doc);
			})
	}); 

	
	 app.put('/userlist/:id', function(req,res)
	{
		var id = req.params.id;
		console.log(req.body.name);
		db.users.findAndModify({
			query: {_id: mongojs.ObjectId(id)},
			update: {$set: {name: req.body.name, email: req.body.email, country: req.body.country}},
			new: true}, function(err,doc)
			{
				res.json(doc);
			});
			
	}); 
	
	app.listen(3000);
	console.log("server is running port 3000");